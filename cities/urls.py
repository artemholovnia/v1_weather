from django.urls.conf import path, re_path

from cities.views import cities, subscriptions, subscribe, unsubscribe, login, logout,\
    readme


app_name = 'cities'
urlpatterns = [
    path('', cities, name='cities-list'),
    path('subscriptions', subscriptions, name='subscriptions-list'),
    re_path(r'^unsubscribe/(?P<id>[0-9]+)/$', unsubscribe, name='unsubscribe'),
    re_path(r'^subscribe/(?P<id>[0-9]+)/$', subscribe, name='subscribe'),

    path('login', login, name='login'),
    path('logout', logout, name='logout'),
    path('readme', readme, name='readme')
]