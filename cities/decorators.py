from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect
from django.urls.base import reverse
from django.utils.safestring import mark_safe

from cities.models import Subscription


def empty_subscriptions(func):
    def wrapper(request):
        if Subscription.objects.filter(user=request.user).exists():
            return func(request)
        return redirect(reverse('cities:cities-list'))
    return wrapper

def subscriptions_limit(func):
    def wrapper(request, *args, **kwargs):
        if Subscription.objects.filter(user=request.user).count() < settings.MAX_SUBSCRIPTIONS_NUM:
            return func(request, *args, **kwargs)
        subscriptions_list_url = reverse('cities:subscriptions-list')
        messages.add_message(request, messages.WARNING, mark_safe(f'Subscription limit exceeded. <a href="{subscriptions_list_url}">Your current subscriptions</a>'))
        return redirect(reverse('cities:cities-list'))
    return wrapper
