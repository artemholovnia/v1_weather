from django import template

register = template.Library()

@register.filter
def filter_by_country(queryset, country):
    return queryset.filter(country=country)