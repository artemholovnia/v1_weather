from django.contrib import admin

from cities.models import City

class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'temperature')
    search_fields = ('^name', '^country')
    list_per_page = 50

# Register your models here.
admin.site.register(City, CityAdmin)